package com.alexriggs.dev.bukkit.snowtrails;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class SnowTrails extends JavaPlugin implements Listener {
	HashMap<Player, Boolean> stOff = new HashMap<Player, Boolean>();

	@Override
	public void onEnable() {
		this.saveDefaultConfig();
		statsBuildUrlEnable("onEnable");
		getServer().getPluginManager().registerEvents(this, this);
	}

	@Override
	public void onDisable() {
		statsBuildUrlEnable("onDisable");
	} 
	
	public void statsBuildUrlEnable(String action) {
		URI uri;
		URL url;
		try {
			uri = new URI(
					"http",
					"dev.alexriggs.com",
					"/bukkit/saveStats.php",
					"port=" + getServer().getPort()
						+ "&pname=" + getDescription().getName()
						+ "&log_type=enable"
						+ "&playerscount=" + Bukkit.getOnlinePlayers().length
						+ "&bver=" + getServer().getBukkitVersion()
						+ "&pver=" + getDescription().getVersion()
						+ "&action=" + action,
				    null);
			url = uri.toURL();
			sendStats(url);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void statsBuildUrlOnOff(Player player, Integer onoroff) {
		URI uri;
		URL url;
		try {
			uri = new URI(
					"http",
					"dev.alexriggs.com",
					"/bukkit/saveStats.php",
					"port=" + getServer().getPort()
						+ "&pname=" + getDescription().getName()
						+ "&log_type=onoff"
						+ "&playerscount=" + Bukkit.getOnlinePlayers().length
						+ "&bver=" + getServer().getBukkitVersion()
						+ "&pver=" + getDescription().getVersion()
						+ "&player=" + player
						+ "&command=" + onoroff,
				    null);
			url = uri.toURL();
			sendStats(url);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void statsBuildUrlTrail(Player player) {
		URI uri;
		URL url;
		try {
			uri = new URI(
					"http",
					"dev.alexriggs.com",
					"/bukkit/saveStats.php",
					"port=" + getServer().getPort()
						+ "&pname=" + getDescription().getName()
						+ "&log_type=trail"
						+ "&playerscount=" + Bukkit.getOnlinePlayers().length
						+ "&bver=" + getServer().getBukkitVersion()
						+ "&pver=" + getDescription().getVersion()
						+ "&player=" + player,
				    null);
			url = uri.toURL();
			sendStats(url);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void sendStats(URL URL_builder) {

		if (this.getConfig().getBoolean("statistics")) {
			try {
				URLConnection connection = URL_builder.openConnection();
				connection.connect();
				BufferedReader in = new BufferedReader(new InputStreamReader(
						connection.getInputStream()));
				in.close();
			} catch (MalformedURLException e) {
				getLogger().info("MalformedURLException: " + e);
				e.printStackTrace();
			} catch (IOException e) {
				getLogger().info("IOException: " + e);
				e.printStackTrace();
			}
		}
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		Player player = (Player) sender;
		if (cmd.getName().equalsIgnoreCase("snowtrail")) {
			if (args.length == 0) {
				sender.sendMessage("You must enter a command.");
				return true;
			} else {
				if (args[0].equalsIgnoreCase("off")) {
					stOff.put(player, true);
					sender.sendMessage("Snow trails has now been turned off for you.");
					statsBuildUrlOnOff(player, 0);
					return true;
				} else if (args[0].equalsIgnoreCase("on")) {
					stOff.put(player, false);
					sender.sendMessage("Snow trails has now been turned on for you.");
					statsBuildUrlOnOff(player, 1);
					return true;
				} else {
					sender.sendMessage("Command not found.");
				}
			}
			
		}
		return false;
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();

		if (stOff.get(player) == null) {
			if (this.getConfig().getBoolean("onByDefault")) {
				stOff.put(player, false);
			} else {
				stOff.put(player, true);
			}
		}
		if (!stOff.get(player)) {
			if (player.hasPermission("snowtrails.trail")) {
				Location loc = player.getLocation();
				if (loc.getBlock().isEmpty()) {
					loc.setY(loc.getY() - 1);
					if (isSoil(loc.getBlock())) {
						loc.setY(loc.getY() + 1);
						loc.getBlock().setType(Material.SNOW);
						// Currently disabled because it causes snow
						// to lag badly.
						// statsBuildUrlTrail(player);
					}
				}
			}
		}
	}

	public boolean isSoil(Block block) {
		Material mat = block.getType();

		return (mat == Material.DIRT) || (mat == Material.GRASS)
				|| (mat == Material.SANDSTONE) || (mat == Material.SAND);
	}
}